# NoteListKotlin

After cloning my repository you will be able to run this app.

Instructions:

1, When you open the app you will see list of all notes

2, By clicking on each note, the note is opened as a new screen where it can be edited. Edited notes is saved after click on "Edit" toolbar button or by clicking on back button or by navigating back.

3, By clicking on "plus" button in main activity, user is able to create a new note. New note can be created on the new screen and saved after click on "Edit" toolbar button or by clicking on back button or by navigating back.

4, By long clicking on each note alert dialog is showed and user can delete the note.

