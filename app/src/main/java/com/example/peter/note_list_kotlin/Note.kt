package com.example.peter.note_list_kotlin

import android.os.Parcel
import android.os.Parcelable

import com.google.gson.annotations.SerializedName

/***
 * Model class for a Note
 */
class Note : Parcelable {

    @SerializedName("id")
    var id: Long? = null

    @SerializedName("title")
    var title: String? = null

    constructor(id: Long?, title: String) {
        this.id = id
        this.title = title
    }

    constructor()

    private constructor(parcel: Parcel) {
        id = parcel.readLong()
        title = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id!!)
        dest.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Note> {

        override fun createFromParcel(parcel: Parcel): Note {
            return Note(parcel)
        }

        override fun newArray(size: Int): Array<Note?> {
            return arrayOfNulls(size)
        }

    }
}