package com.example.peter.note_list_kotlin

/***
 * File containing all constants used in a code
 */
object Constants {
    const val CREATE_NEW_NOTE = 1
    const val EDIT_NOTE = 2
    const val NEW_NOTE_SAVED = 10
    const val NOTE_EDITED = 20

    const val TITLE = "title"
    const val NEW_NOTE_PARCELABLE = "new_note_parcelable"
    const val EDIT_NOTE_PARCELABLE = "edit_note_parcelable"
}