package com.example.peter.note_list_kotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NoteDetailActivity : AppCompatActivity() {

    private var progressBar: ProgressBar? = null
    private var noteEditText: EditText? = null
    private var intentNote: Note? = null
    private var editMenuItem: MenuItem? = null
    private var saveMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)

        // obtaining a note to show from the MainActivity
        intentNote = intent.getParcelableExtra(Constants.TITLE)

        // setting toolbar title and back toolbar arrow
        supportActionBar?.setTitle(if (intentNote == null) R.string.new_note_toolbar_title else R.string.edit_note_toolbar_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //editText initialization
        noteEditText = findViewById(R.id.edit_text)
        noteEditText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                // enable or disable toolbar menu item
                if (editable.toString().isEmpty() || intentNote?.title == editable.toString()) {
                    editMenuItem?.isEnabled = false
                    saveMenuItem?.isEnabled = false
                } else {
                    editMenuItem?.isEnabled = true
                    saveMenuItem?.isEnabled = true
                }
            }
        })
        noteEditText?.setText(intentNote?.title)
        noteEditText?.hint = if (intentNote == null) getString(R.string.new_note_hint) else ""

        progressBar = findViewById(R.id.progressBar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // showing proper text for toolbar menu item
        if (intentNote == null) {
            menuInflater.inflate(R.menu.save, menu)
        } else {
            menuInflater.inflate(R.menu.edit, menu)
        }
        editMenuItem = menu.findItem(R.id.action_edit)
        saveMenuItem = menu.findItem(R.id.action_save)
        editMenuItem?.isEnabled = false
        saveMenuItem?.isEnabled = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // handle action after click on menu item
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_save, R.id.action_edit -> {
                processNote(false)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (!processNote(true)) {
            super.onBackPressed()
        } else {
            showAlertDialog()
        }
    }

    /***
     * Method for showing alert dialog when user wants to go back from the screen without saving changes
     */
    private fun showAlertDialog() {
        val builder = AlertDialog.Builder(this@NoteDetailActivity)
        builder.setMessage(R.string.confirm_changes_dialog_message)
        builder.setPositiveButton(R.string.confirm_changes_positive_button) { dialogInterface, i -> processNote(false) }
        builder.setNegativeButton(R.string.confirm_changes_negative_button) { dialogInterface, i -> finish() }
        builder.show()
    }

    /***
     * Method for calling new note creation service
     * @param newNote object of a note which is created
     */
    private fun saveNewNote(newNote: Note) {
        progressBar?.visibility = View.VISIBLE
        this.noteEditText?.visibility = View.GONE
        HttpRequest.createService(RESTService::class.java).createNewNote(newNote).enqueue(object : Callback<Note> {
            override fun onResponse(call: Call<Note>, response: Response<Note>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        setResult(Constants.NEW_NOTE_SAVED, Intent().putExtra(Constants.NEW_NOTE_PARCELABLE, response.body()))
                    }
                } else {
                    Toast.makeText(this@NoteDetailActivity, getString(R.string.new_note_save_failure), Toast.LENGTH_LONG).show()
                }
                finish()
            }

            override fun onFailure(call: Call<Note>, t: Throwable) {
                Toast.makeText(this@NoteDetailActivity, getString(R.string.server_error), Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    /***
     * Method for calling service to edit note
     * @param note object of a note which is edited
     */
    private fun editNote(note: Note) {
        progressBar?.visibility = View.VISIBLE
        this.noteEditText?.visibility = View.GONE
        HttpRequest.createService(RESTService::class.java).editNote(note.id, note).enqueue(object : Callback<Note> {
            override fun onResponse(call: Call<Note>, response: Response<Note>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        setResult(Constants.NOTE_EDITED, Intent().putExtra(Constants.EDIT_NOTE_PARCELABLE, response.body()))
                    }
                } else {
                    Toast.makeText(this@NoteDetailActivity, getString(R.string.edit_note_failure), Toast.LENGTH_LONG).show()
                }
                finish()
            }

            override fun onFailure(call: Call<Note>, t: Throwable) {
                Toast.makeText(this@NoteDetailActivity, getString(R.string.server_error), Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    /***
     * Method which makes a decision which service will be called
     * @param onBackPressed indicates different behavior in case the method is called immediately after backPressed
     */
    private fun processNote(onBackPressed: Boolean): Boolean {
        // if noteEditText is not empty
        if (!TextUtils.isEmpty(noteEditText?.text.toString())) {
            // if there was not any text within noteEditText after creation of the activity -> case of creating a new note
            if (intentNote == null) {
                if (!onBackPressed) {
                    val newNote = Note()
                    newNote.title = noteEditText?.text.toString()
                    saveNewNote(newNote)
                }
                return true
            // if a text within noteEditText has not been changed -> case of editing a note
            } else if (intentNote?.title != noteEditText?.text.toString()) {
                if (!onBackPressed) {
                    intentNote?.title = noteEditText?.text.toString()
                    editNote(intentNote!!)
                }
                return true
            }
        }
        return false
    }
}