package com.example.peter.note_list_kotlin

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    private var pullToRefresh: SwipeRefreshLayout? = null
    private var progressBar: ProgressBar? = null
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: RecyclerView.Adapter<*>? = null
    private var positionToEdit: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //// handle automatic loading of all notes after pulling list of notes down
        pullToRefresh = findViewById(R.id.pullToRefresh)
        pullToRefresh?.setOnRefreshListener { loadNotes(false) }

        progressBar = findViewById(R.id.progressBar)

        //// RecyclerView setup
        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView?.setHasFixedSize(false)
        val mLayoutManager = LinearLayoutManager(this)
        mRecyclerView?.layoutManager = mLayoutManager

        //// FloatingActionButton setup
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { startActivityForResult(Intent(this@MainActivity, NoteDetailActivity::class.java), Constants.CREATE_NEW_NOTE) }

        loadNotes(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            // showing of just created or edited note
            if (requestCode == Constants.CREATE_NEW_NOTE && resultCode == Constants.NEW_NOTE_SAVED) {
                (mAdapter as NotesAdapter).addItem(data.getParcelableExtra<Parcelable>(Constants.NEW_NOTE_PARCELABLE) as Note)
            } else if (requestCode == Constants.EDIT_NOTE && resultCode == Constants.NOTE_EDITED) {
                (mAdapter as NotesAdapter).editItem(positionToEdit, data.getParcelableExtra<Parcelable>(Constants.EDIT_NOTE_PARCELABLE) as Note)
            }
        }
    }

    /***
     * method for loading all notes from server
     * @param showProgressDialog indicates whether a visibility of progressBar is expected or not
     */
    private fun loadNotes(showProgressDialog: Boolean) {
        if (showProgressDialog) {
            showProgressBar(true)
        }
        HttpRequest.createService(RESTService::class.java).getNotes().enqueue(object : Callback<List<Note>> {
            override fun onResponse(call: Call<List<Note>>, response: Response<List<Note>>) {
                pullToRefresh?.isRefreshing = false
                showProgressBar(false)
                if (response.isSuccessful) {
                    // adapter initialization
                    mAdapter = NotesAdapter(response.body() as ArrayList<Note>)
                    mRecyclerView?.adapter = mAdapter
                } else {
                    Toast.makeText(this@MainActivity, R.string.loading_notes_failure, Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<List<Note>>, t: Throwable) {
                pullToRefresh?.isRefreshing = false
                showProgressBar(false)
                Toast.makeText(this@MainActivity, R.string.server_error, Toast.LENGTH_LONG).show()
            }
        })
    }

    // RecyclerView adapter
    inner class NotesAdapter
    (private val mDataSet: ArrayList<Note>) : RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {

        init {
            // reverse order to show last added items on the top of a list
            mDataSet.reverse()
        }

        // Create new view for each note
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesAdapter.NotesViewHolder {
            return NotesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.note, parent, false))
        }

        // setting title to each note of dataSet
        override fun onBindViewHolder(holder: NotesAdapter.NotesViewHolder, position: Int) {
            holder.noteText.text = mDataSet[position].title
        }

        // Return the size of a dataSet (number of notes to show)
        override fun getItemCount(): Int {
            return mDataSet.size
        }

        /**
         * show changes in RecyclerView after deletion of a note
         */
        fun deleteItem(position: Int) {
            mDataSet.removeAt(position)
            notifyItemRemoved(position)
        }

        /**
         * show changes in RecyclerView after adding a new note
         */
        fun addItem(note: Note?) {
            if (note != null) {
                mDataSet.add(0, note)
                notifyItemInserted(0)
                mRecyclerView?.smoothScrollToPosition(0)
            }
        }

        /**
         * show changes in RecyclerView after editing a note
         */
        fun editItem(position: Int, note: Note?) {
            if (note != null) {
                mDataSet[position].title = note.title
                notifyItemChanged(position, note)
            }
        }

        //view holder class for adapter
        inner class NotesViewHolder(v: View) : RecyclerView.ViewHolder(v) {
            var noteWrapper: LinearLayout
            var noteText: TextView

            init {
                noteWrapper = v.findViewById(R.id.note_wrapper)
                noteText = v.findViewById(R.id.note_text)

                // navigate to new activity for editing a note after click on the note
                noteWrapper.setOnClickListener {
                    val intent = Intent(this@MainActivity, NoteDetailActivity::class.java)
                    intent.putExtra(Constants.TITLE, mDataSet[adapterPosition])
                    startActivityForResult(intent, Constants.EDIT_NOTE)
                    positionToEdit = adapterPosition
                }

                // show deletion alert dialog after long click on the note
                noteWrapper.setOnLongClickListener {
                    val builder = AlertDialog.Builder(this@MainActivity)
                    builder.setMessage(R.string.delete_dialog_title)
                    builder.setPositiveButton(R.string.delete_dialog_positive_button) { dialogInterface, i -> deleteNote(adapterPosition, mDataSet[adapterPosition]) }
                    builder.setNegativeButton(R.string.delete_dialog_negative_button, null)
                    builder.show()
                    true
                }
            }
        }
    }

    /***
     * Method for calling deletion service
     * @param position item position
     * @param note note for deletion
     */
    private fun deleteNote(position: Int, note: Note) {
        showProgressBar(true)
        HttpRequest.createService(RESTService::class.java).deleteNote(note.id).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                showProgressBar(false)
                if (response.isSuccessful) {
                    (mAdapter as NotesAdapter).deleteItem(position)
                } else {
                    Toast.makeText(this@MainActivity, getString(R.string.delete_note_failure), Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                showProgressBar(false)
                Toast.makeText(this@MainActivity, getString(R.string.server_error), Toast.LENGTH_LONG).show()
            }
        })
    }

    /***
     * Method for switching visibility of progress bar and recycler view
     */
    private fun showProgressBar(show: Boolean) {
        if (show) {
            progressBar?.visibility = View.VISIBLE
            mRecyclerView?.visibility = View.GONE
        } else {
            progressBar?.visibility = View.GONE
            mRecyclerView?.visibility = View.VISIBLE
        }
    }
}
