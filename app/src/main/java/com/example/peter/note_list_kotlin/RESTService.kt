package com.example.peter.note_list_kotlin

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

/***
 * Interface for calling services using Retrofit
 */
interface RESTService {
    @GET("notes")
    fun getNotes(): Call<List<Note>>

    @GET("notes/{id}")
    fun getNoteById(@Path("id") id: Long?): Call<Note>

    @POST("notes")
    fun createNewNote(@Body newNote: Note): Call<Note>

    @PUT("notes/{id}")
    fun editNote(@Path("id") id: Long?, @Body note: Note): Call<Note>

    @DELETE("notes/{id}")
    fun deleteNote(@Path("id") id: Long?): Call<Void>
}