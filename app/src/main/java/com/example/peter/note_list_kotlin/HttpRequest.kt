package com.example.peter.note_list_kotlin

import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/***
 * Initialization of server communication using Retrofit
 */
object HttpRequest {

    private var sHttpclient: OkHttpClient? = null
    private var sRetrofit: Retrofit? = null

    private const val TIMEOUT_VALUE = 30

    fun <S> createService(serviceClass: Class<S>): S {
        if (sRetrofit == null) {
            sRetrofit = getRetrofit()
        }

        return sRetrofit!!.create(serviceClass)
    }

    private fun getRetrofit(): Retrofit {
        if (sHttpclient == null) {
            sHttpclient = getHttpClient()
        }

        return Retrofit.Builder()
                .baseUrl("http://private-9aad-note10.apiary-mock.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(sHttpclient!!)
                .build()
    }

    private fun getHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(TIMEOUT_VALUE.toLong(), TimeUnit.SECONDS)
        httpClient.readTimeout(TIMEOUT_VALUE.toLong(), TimeUnit.SECONDS)
        return httpClient.build()
    }
}